#include "mytimer.h"
#include <QDebug>

MyTimer::MyTimer(QObject *parent)
    : QObject{parent}
{
    // create a timer
    timer = new QTimer(this);

    // setup signal and slot
    // connect(timer, SIGNAL(timeout()), this, SLOT(TimerSlot()));
    connect(timer, &QTimer::timeout, this, &MyTimer::TimerSlot);

    // msec
    timer->start(1000);
}

void MyTimer::TimerSlot()
{
    qDebug() << "Timer...";
}
