#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // create objects for the label and progress bar
    statusLabel = new QLabel(this);
    statusProgressBar = new QProgressBar(this);

    // set text for the label
    statusLabel->setText("Status Label");

    // make progress bar text invisible
    statusProgressBar->setTextVisible(false);

    // add the two controls to the status bar
    ui->statusbar->addPermanentWidget(statusLabel);
    ui->statusbar->addPermanentWidget(statusProgressBar, 1);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionMenuAction_triggered()
{
    // showMessage(const QString & message, int timeout = 0)
    ui->statusbar->showMessage("Status");

    // When the action triggered, set the progress bar at 51%
    statusProgressBar->setValue(51);
}
