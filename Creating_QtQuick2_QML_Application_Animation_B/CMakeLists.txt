cmake_minimum_required(VERSION 3.16)

project(Creating_QtQuick2_QML_Application_Animation_B VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOMOC ON)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 6.2 COMPONENTS Quick REQUIRED)

qt_add_executable(appCreating_QtQuick2_QML_Application_Animation_B
    main.cpp
    resources.qrc
)

qt_add_qml_module(appCreating_QtQuick2_QML_Application_Animation_B
    URI Creating_QtQuick2_QML_Application_Animation_B
    VERSION 1.0
    QML_FILES main.qml 
)

set_target_properties(appCreating_QtQuick2_QML_Application_Animation_B PROPERTIES
    MACOSX_BUNDLE_GUI_IDENTIFIER my.example.com
    MACOSX_BUNDLE_BUNDLE_VERSION ${PROJECT_VERSION}
    MACOSX_BUNDLE_SHORT_VERSION_STRING ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}
    MACOSX_BUNDLE TRUE
    WIN32_EXECUTABLE TRUE
)

target_link_libraries(appCreating_QtQuick2_QML_Application_Animation_B
    PRIVATE Qt6::Quick)

install(TARGETS appCreating_QtQuick2_QML_Application_Animation_B
    BUNDLE DESTINATION .
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
