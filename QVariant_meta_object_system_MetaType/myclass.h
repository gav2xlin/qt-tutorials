#ifndef MYCLASS_H
#define MYCLASS_H

#include <QMetaType>
#include <QString>

class MyClass
{
public:
    MyClass();
    QString mName;
};

Q_DECLARE_METATYPE(MyClass)

/*struct MyStruct
{
    int i;
    ...
};

Q_DECLARE_METATYPE(MyStruct)*/

/*namespace MyNamespace
{
    ...
}

Q_DECLARE_METATYPE(MyNamespace::MyStruct)*/

#endif // MYCLASS_H
