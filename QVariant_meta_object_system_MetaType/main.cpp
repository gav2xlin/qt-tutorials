#include <QCoreApplication>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include <QStringList>
#include <QVariant>
#include "myclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << "Writing...";
    QFile file("file.txt");
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);   // we will serialize the data into the file

    QVariant v(123);                // The variant now contains an int
    int x = v.toInt();              // x = 123
    out << v;                       // Writes a type tag and an int to out
    qDebug() << v;
    v = QVariant("hello");          // The variant now contains a QByteArray
    v = QVariant(QObject::tr("hello"));      // The variant now contains a QString
    int y = v.toInt();              // y = 0 since v cannot be converted to an int
    QString s = v.toString();       // s = tr("hello")  (see QObject::tr())
    out << v;
    qDebug() << v;
    file.flush();
    file.close();

    qDebug() << "Reading...";
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);          // (opening the previously written stream)
    in >> v;                        // Reads an Int variant
    int z = v.toInt();              // z = 123
    qDebug() << v;

    in >> v;

    // just checking the possibility
    // does not guarantee the valid conversion
    if(v.canConvert<QStringList>()) {
        qDebug() << v.toStringList();
    }

    file.close();

    /*MyClass mClass;
    QVariant v = QVariant::fromValue(mClass);*/

    /*MyCustomStruct s;
    return QVariant::fromValue(s);*/

    /*int id = QMetaType::type("MyClass");
    if (id != QMetaType::UnknownType) {
        void *myClassPtr = QMetaType::create(id);
        ...
            QMetaType::destroy(id, myClassPtr);
        myClassPtr = 0;
    }*/

    /*MyStruct s;
    QVariant var;
    var.setValue(s); // copy s into the variant
    ...
    // retrieve the value
    MyStruct s2 = var.value<MyStruct>();*/

    /*MyClass mClass;
    mClass.name = "Debussy";

    // put a class into QVariant
    QVariant v = QVariant::fromValue(mClass);

    // What's the type?
    // It's MyClass, and it's been registered
    // by adding macro in "myclass.h"
    MyClass vClass = v.value<MyClass>();

    qDebug() << vClass.name;*/

    return a.exec();
}
