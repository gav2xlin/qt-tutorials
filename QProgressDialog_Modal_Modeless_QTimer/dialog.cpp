#include "dialog.h"
#include "./ui_dialog.h"
#include <QProgressDialog>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_modalButton_clicked()
{
    int numTasks = 100000;
    QProgressDialog progress("Task in progress...", "Cancel", 0, numTasks, this);
    progress.setWindowModality(Qt::WindowModal);

    for (int i = 0; i < numTasks; i++) {
        progress.setValue(i);

        if (progress.wasCanceled())
            break;
    }
    progress.setValue(numTasks);
}


void Dialog::on_modelessButton_clicked()
{
    myTask = new MyTask;
}
