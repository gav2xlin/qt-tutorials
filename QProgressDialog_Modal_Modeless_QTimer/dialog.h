#ifndef DIALOG_H
#define DIALOG_H

#include "mytask.h"
#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

private slots:
    void on_modalButton_clicked();

    void on_modelessButton_clicked();

private:
    Ui::Dialog *ui;

    MyTask *myTask;
};
#endif // DIALOG_H
