#include <QCoreApplication>
#include <QDir>
#include <QDebug>
#include <QString>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // forward slash for directory separator
    QDir dir1("mydir");
    QDir dir2("mydir/test");

    qDebug() << dir1.exists() << dir2.exists();

    QDir dir3;
    foreach(QFileInfo item, dir3.drives())
    {
        qDebug() << item.absoluteFilePath();
    }

    {
        QString path = "mydir/test";

        QDir dir(path);
        if(!dir.exists())
        {
            qDebug() << "Creating " << path << "directory";
            dir.mkpath(path);
        }
        else
        {
            qDebug() << path << " already exists";
        }
    }

    {
        QString path = ".";
        QDir dir(path);

        foreach(QFileInfo item, dir.entryInfoList())
        {
            if(item.isDir())
                qDebug() << "Dir: " << item.absoluteFilePath();
            if(item.isFile())
                qDebug() << "File: " << item.absoluteFilePath();
        }
    }

    return a.exec();
}
