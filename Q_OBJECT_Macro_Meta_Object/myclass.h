class MyClass : public QObject
{
    Q_OBJECT

public:
    MyClass(QObject *parent = 0);
    ~MyClass();

signals:
    void mySignal();

public slots:
    void mySlot();
};

// moc - Meta-Object Compiler
// QObject::connect(sender, signal, receiver, slot):

