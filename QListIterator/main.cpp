#include <QCoreApplication>
#include <QList>
#include <QDebug>
#include <QListIterator>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<int> List;

    for(int i = 0; i < 10; ++i) List.append(i);

    // The QListIterator constructor takes a QList
    // as argument. After construction, the iterator
    // is located at the very beginning of the list
    // (before the first item)
    QListIterator<int> iter(List);

    qDebug() << "Forward...";
    while(iter.hasNext())
    {
        // The next() function returns the next item
        // in the list and advances the iterator.
        qDebug() << "peek previous ..." << iter.peekPrevious();
        qDebug() << iter.next();
    }

    qDebug() << "Backward...";
    while(iter.hasPrevious())
    {
        // The next() function returns the next item
        // in the list and advances the iterator.
        qDebug() << "peek next ..." << iter.peekNext();
        qDebug() << iter.previous();
    }

    return a.exec();
}
