#include "listwidgetdialog.h"
#include "./ui_listwidgetdialog.h"
#include <QListWidgetItem>

ListWidgetDialog::ListWidgetDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::ListWidgetDialog)
{
    ui->setupUi(this);

    // populate the items of the list
    for(int i = 0; i < 10; i++)
    {
        ui->listWidget->addItem("Item " + QString::number(i));
    }
}

ListWidgetDialog::~ListWidgetDialog()
{
    delete ui;
}


void ListWidgetDialog::on_pushButton_clicked()
{
    // Get the pointer to the currently selected item.
    QListWidgetItem *item = ui->listWidget->currentItem();

    // Set the text color and its background color using the pointer to the item.
    item->setForeground(Qt::yellow);
    item->setBackground(Qt::red);
}
