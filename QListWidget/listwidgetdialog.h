#ifndef LISTWIDGETDIALOG_H
#define LISTWIDGETDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class ListWidgetDialog; }
QT_END_NAMESPACE

class ListWidgetDialog : public QDialog
{
    Q_OBJECT

public:
    ListWidgetDialog(QWidget *parent = nullptr);
    ~ListWidgetDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ListWidgetDialog *ui;
};
#endif // LISTWIDGETDIALOG_H
