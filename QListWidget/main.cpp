#include "listwidgetdialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ListWidgetDialog w;
    w.show();
    return a.exec();
}
