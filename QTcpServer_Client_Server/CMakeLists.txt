cmake_minimum_required(VERSION 3.14)

project(QTcpServer_Client_Server LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core Network)

add_executable(QTcpServer_Client_Server
  main.cpp mytcpserver.h mytcpserver.cpp
)
target_link_libraries(QTcpServer_Client_Server Qt${QT_VERSION_MAJOR}::Core Qt${QT_VERSION_MAJOR}::Network)

install(TARGETS QTcpServer_Client_Server
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
