#ifndef TOOLTIPDIALOG_H
#define TOOLTIPDIALOG_H

#include <QDialog>

QT_BEGIN_NAMESPACE
namespace Ui { class TooltipDialog; }
QT_END_NAMESPACE

class TooltipDialog : public QDialog
{
    Q_OBJECT

public:
    TooltipDialog(QWidget *parent = nullptr);
    ~TooltipDialog();

private:
    Ui::TooltipDialog *ui;
};
#endif // TOOLTIPDIALOG_H
