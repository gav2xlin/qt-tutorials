#include "tooltipdialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TooltipDialog w;
    w.show();
    return a.exec();
}
