#include "tooltipdialog.h"
#include "./ui_tooltipdialog.h"

TooltipDialog::TooltipDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::TooltipDialog)
{
    ui->setupUi(this);

    ui->pushButton->setToolTip(
        "<h2><b><font color='red'>MyList</font></b></h2>"
        "<ol>"
        "<li>First</li>"
        "<li>Second</li>"
        "<li>Third</li>"
        "</ol>"
        );

    ui->pushButton_2->setToolTip("<img src=':/images/book.png'/> Book");
}

TooltipDialog::~TooltipDialog()
{
    delete ui;
}
