#include <QCoreApplication>
#include <QLinkedList>
#include <QDebug>
#include <QString>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QLinkedList<QString> list;

    list << "A" << "B" << "C";
    list.append("D");
    list.append("E");
    list.append("F");

    return a.exec();
}
