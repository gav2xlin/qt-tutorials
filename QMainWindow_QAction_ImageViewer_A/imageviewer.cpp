#include "imageviewer.h"
#include "./ui_imageviewer.h"

ImageViewer::ImageViewer(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ImageViewer)
{
    ui->setupUi(this);

    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    setCentralWidget(scrollArea);

    setWindowTitle(tr("Image Viewer"));
    resize(500, 400);

    /*openAct = new QAction(tr("&Open...;"), this);
    openAct->setShortcut(tr("Ctrl+O"));

    printAct = new QAction(tr("&Print...;"), this);
    printAct->setShortcut(tr("Ctrl+P"));
    printAct->setEnabled(false);

    exitAct = new QAction(tr("E&xit;"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));

    zoomInAct = new QAction(tr("Zoom &In; (25%)"), this);
    zoomInAct->setShortcut(tr("Ctrl+="));   //(Ctrl)(+)
    zoomInAct->setEnabled(false);

    zoomOutAct = new QAction(tr("Zoom &Out; (25%)"), this);
    zoomOutAct->setShortcut(tr("Ctrl+-"));  //(Ctrl)(-)
    zoomOutAct->setEnabled(false);

    normalSizeAct = new QAction(tr("&Normal; Size"), this);
    normalSizeAct->setShortcut(tr("Ctrl+S"));
    normalSizeAct->setEnabled(false);

    fitToWindowAct = new QAction(tr("&Fit; to Window"), this);
    fitToWindowAct->setEnabled(false);
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setShortcut(tr("Ctrl+F"));

    aboutAct = new QAction(tr("&About;"), this);

    aboutQtAct = new QAction(tr("About &Qt;"), this);

    fileMenu = new QMenu(tr("&File;"), this);
    fileMenu->addAction(openAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    viewMenu = new QMenu(tr("&View;"), this);
    viewMenu->addAction(zoomInAct);
    viewMenu->addAction(zoomOutAct);
    viewMenu->addAction(normalSizeAct);
    viewMenu->addSeparator();
    viewMenu->addAction(fitToWindowAct);

    helpMenu = new QMenu(tr("&Help;"), this);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(viewMenu);
    menuBar()->addMenu(helpMenu);*/
}

ImageViewer::~ImageViewer()
{
    delete ui;
}
