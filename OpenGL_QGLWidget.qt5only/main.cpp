#include "widget.h"

#include <QApplication>
#include <QScreen>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Widget window;
    window.resize(window.sizeHint());
    QScreen *screen = QGuiApplication::primaryScreen();
    int desktopArea = screen->geometry().width() *
                      screen->geometry().height();
    int widgetArea = window.width() * window.height();

    window.setWindowTitle("OpenGL with Qt");

    if (((float)widgetArea / (float)desktopArea) < 0.75f)
    {
        window.show();
    }
    else
    {
        window.showMaximized();
    }

    return a.exec();
}
