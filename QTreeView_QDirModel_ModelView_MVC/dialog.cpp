#include "dialog.h"
#include "./ui_dialog.h"
#include <QTreeView>
#include <QInputDialog>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // Create and populate our model
    // model = new QDirModel(this);
    model = new QFileSystemModel(this);
    model->setRootPath(QDir::currentPath());

    // Enable modifying file system
    model->setReadOnly(false);

    // Tie TreeView with DirModel
    // QTreeView::setModel(QAbstractItemModel * model)
    // Reimplemented from QAbstractItemView::setModel().
    ui->treeView->setModel(model);

    // ui->treeView->setRootIndex(model->index(QDir::currentPath()));
}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::on_pushButton_clicked()
{
    // Make directory
    QModelIndex index = ui->treeView->currentIndex();
    if(!index.isValid()) return;

    QString name  = QInputDialog::getText(this, "Name", "Enter a name");

    if(name.isEmpty()) return;

    model->mkdir(index, name);
}

void Dialog::on_pushButton_2_clicked()
{
    // Delete directory

    // Get the current selection
    QModelIndex index = ui->treeView->currentIndex();
    if(!index.isValid()) return;

    if(model->fileInfo(index).isDir())
    {
        // directory
        model->rmdir(index);
    }
    else
    {
        // file
        model->remove(index);
    }
}
