#include <QCoreApplication>
#include <QStringList>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // A QString
    QString str = "A,B,C,D,E,F,G";
    qDebug() << "QString str = " << str;

    QStringList list;
    qDebug() << "split str using ',' as a delimeter";
    list = str.split(",");
    qDebug() << "List = " << list;
    foreach(QString item, list) qDebug() << "List items = " << item;

    qDebug() << "Replace one of the List item";
    list.replaceInStrings("C","CCC");
    qDebug() << "List = " << list;

    qDebug() << "Join all items in the List";
    QString joinedString = list.join(", ");
    qDebug() << "joined string = " << joinedString;

    return a.exec();
}
