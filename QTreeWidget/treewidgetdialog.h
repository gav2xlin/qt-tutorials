#ifndef TREEWIDGETDIALOG_H
#define TREEWIDGETDIALOG_H

#include <QDialog>
#include <QTreeWidget>
#include <QBrush>

QT_BEGIN_NAMESPACE
namespace Ui { class TreeWidgetDialog; }
QT_END_NAMESPACE

class TreeWidgetDialog : public QDialog
{
    Q_OBJECT

public:
    TreeWidgetDialog(QWidget *parent = nullptr);
    ~TreeWidgetDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::TreeWidgetDialog *ui;

    void addTreeRoot(QString name, QString description);
    void addTreeChild(QTreeWidgetItem *parent, QString name, QString description);
};
#endif // TREEWIDGETDIALOG_H
