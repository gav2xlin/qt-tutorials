#include "treewidgetdialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TreeWidgetDialog w;
    w.show();
    return a.exec();
}
