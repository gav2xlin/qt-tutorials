#include "imageviewer.h"
#include "./ui_imageviewer.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QPrintDialog>
#include <QScrollBar>
#include <QPainter>

ImageViewer::ImageViewer(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::ImageViewer)
{
    ui->setupUi(this);

    imageLabel = new QLabel;
    imageLabel->setBackgroundRole(QPalette::Base);
    imageLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageLabel->setScaledContents(true);

    scrollArea = new QScrollArea;
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageLabel);
    setCentralWidget(scrollArea);

    setWindowTitle(tr("Image Viewer"));
    resize(500, 400);

    /*openAct = new QAction(tr("&Open...;"), this);
    openAct->setShortcut(tr("Ctrl+O"));

    printAct = new QAction(tr("&Print...;"), this);
    printAct->setShortcut(tr("Ctrl+P"));
    printAct->setEnabled(false);

    exitAct = new QAction(tr("E&xit;"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));

    zoomInAct = new QAction(tr("Zoom &In; (25%)"), this);
    zoomInAct->setShortcut(tr("Ctrl+="));   //(Ctrl)(+)
    zoomInAct->setEnabled(false);

    zoomOutAct = new QAction(tr("Zoom &Out; (25%)"), this);
    zoomOutAct->setShortcut(tr("Ctrl+-"));  //(Ctrl)(-)
    zoomOutAct->setEnabled(false);

    normalSizeAct = new QAction(tr("&Normal; Size"), this);
    normalSizeAct->setShortcut(tr("Ctrl+S"));
    normalSizeAct->setEnabled(false);

    fitToWindowAct = new QAction(tr("&Fit; to Window"), this);
    fitToWindowAct->setEnabled(false);
    fitToWindowAct->setCheckable(true);
    fitToWindowAct->setShortcut(tr("Ctrl+F"));

    aboutAct = new QAction(tr("&About;"), this);

    aboutQtAct = new QAction(tr("About &Qt;"), this);

    fileMenu = new QMenu(tr("&File;"), this);
    fileMenu->addAction(openAct);
    fileMenu->addAction(printAct);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAct);

    viewMenu = new QMenu(tr("&View;"), this);
    viewMenu->addAction(zoomInAct);
    viewMenu->addAction(zoomOutAct);
    viewMenu->addAction(normalSizeAct);
    viewMenu->addSeparator();
    viewMenu->addAction(fitToWindowAct);

    helpMenu = new QMenu(tr("&Help;"), this);
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);

    menuBar()->addMenu(fileMenu);
    menuBar()->addMenu(viewMenu);
    menuBar()->addMenu(helpMenu);*/

    /*connect(openAct, SIGNAL(triggered()), this, SLOT(open()));
    connect(printAct, SIGNAL(triggered()), this, SLOT(print()));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));
    connect(zoomInAct, SIGNAL(triggered()), this, SLOT(zoomIn()));
    connect(zoomOutAct, SIGNAL(triggered()), this, SLOT(zoomOut()));
    connect(normalSizeAct, SIGNAL(triggered()), this, SLOT(normalSize()));
    connect(fitToWindowAct, SIGNAL(triggered()), this, SLOT(fitToWindow()));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));*/

    openAct = ui->openAct;
    printAct = ui->printAct;
    exitAct = ui->exitAct;
    zoomInAct = ui->zoomInAct;
    zoomOutAct = ui->zoomOutAct;
    normalSizeAct = ui->normalSizeAct;
    fitToWindowAct = ui->fitToWindowAct;
    aboutAct = ui->aboutAct;
    aboutQtAct = ui->aboutQtAct;
}

ImageViewer::~ImageViewer()
{
    delete ui;
}

void ImageViewer::on_openAct_triggered()
{
    qDebug() << "open()";
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath());

    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1.").arg(fileName));
            return;
        }
        imageLabel->setPixmap(QPixmap::fromImage(image));
        scaleFactor = 1.0;

        printAct->setEnabled(true);
        fitToWindowAct->setEnabled(true);
        updateActions();

        if (!fitToWindowAct->isChecked())
            imageLabel->adjustSize();
            // imageLabel->resize(imageLabel->pixmap()->size());
    }
}


void ImageViewer::on_zoomInAct_triggered()
{
    scaleImage(1.25);
}


void ImageViewer::on_zoomOutAct_triggered()
{
    scaleImage(0.8);
}


void ImageViewer::on_normalSizeAct_triggered()
{
    imageLabel->adjustSize();
    scaleFactor = 1.0;
}


void ImageViewer::on_fitToWindowAct_triggered()
{
    bool fitToWindow = fitToWindowAct->isChecked();
    scrollArea->setWidgetResizable(fitToWindow);
    if (!fitToWindow) {
        on_normalSizeAct_triggered();
    }
    updateActions();
}

void ImageViewer::updateActions()
{
    zoomInAct->setEnabled(!fitToWindowAct->isChecked());
    zoomOutAct->setEnabled(!fitToWindowAct->isChecked());
    normalSizeAct->setEnabled(!fitToWindowAct->isChecked());
}

void ImageViewer::scaleImage(double factor)
{
    Q_ASSERT(!imageLabel->pixmap().isNull());
    scaleFactor *= factor;
    imageLabel->resize(scaleFactor * imageLabel->pixmap().size());

    adjustScrollBar(scrollArea->horizontalScrollBar(), factor);
    adjustScrollBar(scrollArea->verticalScrollBar(), factor);

    zoomInAct->setEnabled(scaleFactor < 3.0);
    zoomOutAct->setEnabled(scaleFactor > 0.333);
}

void ImageViewer::adjustScrollBar(QScrollBar *scrollBar, double factor)
{
    scrollBar->setValue(int(factor * scrollBar->value() + ((factor - 1) * scrollBar->pageStep()/2)));
    // scrollBar->setValue(int(factor * scrollBar->value()));
}

void ImageViewer::on_printAct_triggered()
{
    Q_ASSERT(!imageLabel->pixmap().isNull());
    QPrintDialog dialog(&printer, this);

    if (dialog.exec()) {
        QPainter painter(&printer);
        QRect rect = painter.viewport();
        QSize size = imageLabel->pixmap().size();
        size.scale(rect.size(), Qt::KeepAspectRatio);
        painter.setViewport(rect.x(), rect.y(), size.width(), size.height());
        painter.setWindow(imageLabel->pixmap().rect());
        painter.drawPixmap(0, 0, imageLabel->pixmap());
    }
}
