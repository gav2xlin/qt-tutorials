#include <QCoreApplication>
#include <QDebug>
#include "mythread.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    MyThread mThread;
    qDebug() << "GUI thread before MyThread start()" << app.thread()->currentThreadId();

    mThread.start();

    qDebug() << "GUI thread after start()" << app.thread()->currentThreadId();

    mThread.wait();

    qDebug() << "GUI thread after wait() " << app.thread()->currentThreadId();

    return app.exec();
}
