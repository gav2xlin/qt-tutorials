#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "consumer.h"
#include "producer.h"
#include <QSemaphore>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

public slots:
    void onBufferValueChanged(int);
    void onProducerValueChanged(int);
    void onConsumerValueChanged(int);

private slots:
    void on_startButton_clicked();

private:
    Ui::Dialog *ui;

    Producer *mProducer;
    Consumer *mConsumer;
};
#endif // DIALOG_H
