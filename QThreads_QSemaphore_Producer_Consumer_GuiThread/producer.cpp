#include "producer.h"
#include "common.h"
#include <QRandomGenerator>

Producer::Producer(QObject *parent) :
    QThread(parent)
{
}

void Producer::run()
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));
#endif
    for (int i = 0; i < DataSize; ++i) {
        freeBytes.acquire();
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
        buffer[i % BufferSize] = "ACGT"[static_cast<int>(QRandomGenerator::global()->generate()) % 4];
#else
        buffer[i % BufferSize] = "ACGT"[(int)qrand() % 4];
#endif
        usedBytes.release();
        if(i % 20 == 0) {
            emit bufferFillCountChanged(usedBytes.available());
        }
        emit producerCountChanged(i);
    }
}
