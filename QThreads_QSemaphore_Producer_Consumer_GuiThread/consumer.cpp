#include "consumer.h"
#include "common.h"
#include <QTextStream>

Consumer::Consumer(QObject *parent) :
    QThread(parent)
{
}

void Consumer::run()
{
    QTextStream out(stderr);
    for (int i = 0; i < DataSize; ++i) {
        usedBytes.acquire();
        // fprintf(stderr, "%c", buffer[i % BufferSize]);
        out << buffer[i % BufferSize];
        freeBytes.release();
        emit bufferFillCountChanged(usedBytes.available());
        emit consumerCountChanged(i);
    }
    fprintf(stderr, "\n");
}
