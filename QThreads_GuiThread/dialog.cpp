#include "dialog.h"
#include "./ui_dialog.h"
#include "mythread.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);

    // create an instance of MyThread
    /*mThread = new MyThread(this);

    // connect signal/slot
    // connect(mThread, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
    connect(mThread, &MyThread::valueChanged, this, &Dialog::onValueChanged);*/
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_startButton_clicked()
{
    mThread = new MyThread(this);

    // connect signal/slot
    // connect(mThread, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));
    connect(mThread, &MyThread::valueChanged, this, &Dialog::onValueChanged);

    connect(mThread, &MyThread::started, this, &Dialog::onThreadStarted);
    connect(mThread, &MyThread::finished, this, &Dialog::onThreadFinished);
    // connect(mThread, &MyThread::finished, this, &QObject::deleteLater); // crashes the app

    mThread->start();
}

void Dialog::on_stopButton_clicked()
{
    mThread->stop = true;
}

void Dialog::onValueChanged(int count)
{
    ui->label->setText(QString::number(count));
}

void Dialog::onThreadStarted()
{
    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(true);
}

void Dialog::onThreadFinished()
{
    ui->startButton->setEnabled(true);
    ui->stopButton->setEnabled(false);

    delete mThread;
}

