#ifndef DIALOG_H
#define DIALOG_H

#include "mythread.h"
#include <QDialog>
#include <QString>

QT_BEGIN_NAMESPACE
namespace Ui { class Dialog; }
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();

    MyThread *mThread;

private slots:
    void on_startButton_clicked();

    void on_stopButton_clicked();

public slots:
    void onValueChanged(int);
    void onThreadStarted();
    void onThreadFinished();


private:
    Ui::Dialog *ui;
};
#endif // DIALOG_H
