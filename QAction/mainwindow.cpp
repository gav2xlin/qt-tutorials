#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionNew_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("new action");
    msgBox.exec();
}

void MainWindow::on_actionOpen_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("open action");
    msgBox.exec();
}

void MainWindow::on_actionSave_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("save action");
    msgBox.exec();
}

void MainWindow::on_actionPrint_triggered()
{
    QMessageBox msgBox;
    msgBox.setText("print action");
    msgBox.exec();
}

