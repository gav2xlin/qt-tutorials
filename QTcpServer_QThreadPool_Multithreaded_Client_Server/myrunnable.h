#ifndef MYRUNNABLE_H
#define MYRUNNABLE_H

#include <QRunnable>

class MyRunnable : public QRunnable
{
public:
    MyRunnable();

protected:
    void run();

public:
    qintptr socketDescriptor;

};

#endif // MYRUNNABLE_H
